<?php

declare(strict_types = 1);

/**
 * @file
 * Handles the requests of visitors redirected back from SIA Virtual POS.
 */

namespace Drupal\bgcom_payment_gateway\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Drupal\bgcom_payment_gateway\BgcomFailureHandler;
use Drupal\bgcom_payment_gateway\RequestHandlerInterface;
use Drupal\bgcom_payment_gateway\ResponseHandlerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

class SiavposReturnController extends ControllerBase {

  use BgcomFailureHandler;

  /**
   * The logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The request handler service.
   *
   * @var \Drupal\bgcom_payment_gateway\RequestHandlerInterface
   */
  protected $requestHandler;

  /**
   * The response handler service.
   *
   * @var \Drupal\bgcom_payment_gateway\ResponseHandlerInterface
   */
  protected $responseHandler;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    LoggerInterface $logger,
    MessengerInterface $messenger,
    RequestHandlerInterface $request_handler,
    ResponseHandlerInterface $response_handler
  ) {
    $this->logger = $logger;
    $this->messenger = $messenger;
    $this->requestHandler = $request_handler;
    $this->responseHandler = $response_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('logger.channel.bgcom_payment_gateway'),
      $container->get('messenger'),
      $container->get('bgcom_payment_gateway.request_handler'),
      $container->get('bgcom_payment_gateway.response_handler')
    );
  }

  /**
   * Callback for bgcom_payment_gateway.checkout.siavpos_return route.
   */
  public function returnFromSiavpos(Request $request, RouteMatchInterface $route_match) {

    if (!$request->getContent()) {
      return $this->_failureHandler('No body content received in the request.');
    }

    /** @var \Symfony\Component\HttpFoundation\ParameterBag $parameter_bag */
    $parameter_bag = $route_match->getParameters();
    $received_parameters = $parameter_bag->all();

    if (!isset($received_parameters['result'])) {
      $message = 'No result value received among the parameters.';
      $this->logger->error($message);
      $this->messenger->addError($this->t($message));
      return $this->_failureHandler();
    }

    if (!in_array($received_parameters['result'], ['success', 'failure'])) {
      $this->logger->error('Invalid result value ("%value") received in the redirected URL.',
        ['%value' => $received_parameters['result']]
      );
      $this->messenger->addError($this->t('Invalid result value received in the redirected URL.'));
      return $this->_failureHandler();
    }

    if ($received_parameters['result'] === 'failure') {
      $this->logger->error('The user has been redirected back to the URL path of failed transactions.');
      $this->messenger->addError($this->t('External payment gateway service returned with an unspecified error.'));
      return $this->_failureHandler();
    }

    $request_content_deserialized = $this->responseHandler->handleResponse($request->getContent(), 'x');

    $post_data['command'] = 'c';

    if (!array_key_exists('trans_id', $request_content_deserialized)) {
      $this->logger->error(sprintf('Redirected back from remote payment gateway service but no transaction ID detected in the request. Request content was: "%s"', $request_content));
      $this->messenger->addError($this->t('External payment gateway service returned with an unspecified error.'));
      return $this->_failureHandler();
    }

    $post_data['trans_id'] = urldecode($request_content_deserialized['trans_id']);
    $payment_id = \Drupal::entityQuery('commerce_payment')
      ->accessCheck(TRUE)
      ->condition('remote_id', $post_data['trans_id'])
      ->execute();

    if (!$payment_id) {
      return $this->_failureHandler('No payment entity ID was found with the given transaction ID.');
    }

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment_entity */
    $payment_entity = $this->entityTypeManager()
      ->getStorage('commerce_payment')
      ->load(array_shift($payment_id));

    if (!$payment_entity) {
      return $this->_failureHandler('With the given ID no payment entity could be loaded.');
    }

    /** @var \Drupal\commerce_order\Entity\OrderInterface $order_entity */
    $order_entity = $this->entityTypeManager()
      ->getStorage('commerce_order')
      ->load($payment_entity->getOrderId());

    if (!$order_entity) {
      return $this->_failureHandler('With the given ID no order entity could be loaded.');
    }

    /** @var string */
    $post_data['client_ip_addr'] = $order_entity->getIpAddress();
    $response = $this->requestHandler->sendRequest($post_data);

    if (!$response) {
      return $this->_failureHandler('Call of the external payment gateway service returned with an unspecified error.');
    }

    $response_content_deserialized = $this->responseHandler->handleResponse($response, $post_data['command']);

    if ($response_content_deserialized['RESULT'] !== 'OK') {
      return $this->_failureHandler('The payment process has finished successfully, but the transaction resulted a failure.');
    }

    // @todo Confirm whether this is the proper way to toggle state of the payment entity?
    $payment_entity->setState('completed')->save();

    $return_url = Url::fromRoute('commerce_payment.checkout.return', [
      'commerce_order' => $order_entity->id(),
      'step' => 'payment',
    ], ['absolute' => TRUE])->toString();
    return new RedirectResponse($return_url);
  }

}
