<?php

declare(strict_types = 1);

/**
 * @file
 * A centralized unit for building up requests to be sent.
 */

namespace Drupal\bgcom_payment_gateway;

use Drupal\Core\Config\CachedStorage;
use Drupal\Core\Http\ClientFactory;
use Drupal\Core\Messenger\MessengerInterface;
use Psr\Log\LoggerInterface;

class RequestHandler implements RequestHandlerInterface {

  /**
   * The logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The storage instance for reading configuration data.
   *
   * @var \Drupal\Core\Config\StorageInterface
   */
  protected $configStorage;

  /**
   * The HTTP client service.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    LoggerInterface $logger,
    MessengerInterface $messenger,
    CachedStorage $configStorage,
    ClientFactory $http_client
   ) {
    $this->logger = $logger;
    $this->messenger = $messenger;
    $this->configStorage = $configStorage;
    $this->httpClient = $http_client->fromOptions([]);  }

  /**
   * {@inheritdoc}
   */
  public function sendRequest($post_data) {

    foreach ($post_data as $key => $value) {
      $params[] = implode('=', [$key, $value]);
    }
    $post_fields = implode('&', $params);

    // @todo Find a better solution to avoid hardcoding the configuration name.
    $config_entity_content = $this->configStorage->read('commerce_payment.commerce_payment_gateway.siavpos_sms');

    if (!array_key_exists('configuration', $config_entity_content)) {
      $this->logger->error('Could not read data from under the "configuration" key.');
    }

    $plugin_config = $config_entity_content['configuration'];

    // @todo Replace calling cURL to Drupal's HTTP client service powered by Guzzle.
    // Also, figure out, where and how can we configure the 'base_uri' setting of the HTTP client instance object?
    /** @var \Psr\Http\Message\ResponseInterface $response */
    $response = $this->httpClient->request(
      'POST',
      $plugin_config['ecomm_server_url'],
      [
        'cert' => [$plugin_config['cert_url'], $plugin_config['cert_pass']]
      ]
    );

    /** @var resource $curl */
    $curl = curl_init($plugin_config['ecomm_server_url']);
    curl_setopt($curl, CURLOPT_HEADER, 0);
    curl_setopt($curl, CURLOPT_POST, TRUE);
    curl_setopt($curl, CURLOPT_VERBOSE, TRUE);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $post_fields);
    curl_setopt($curl, CURLOPT_SSLCERT, $plugin_config['cert_url']);
    curl_setopt($curl, CURLOPT_CAINFO, $plugin_config['cert_url']);
    curl_setopt($curl, CURLOPT_SSLKEYPASSWD, $plugin_config['cert_pass']);
    $result = curl_exec($curl);

    if ($result === FALSE || curl_error($curl)) {
      $message = 'An error happened during the call of the external payment gateway service.';
      $this->logger->error($message . ' The curl error was: ' . curl_error($curl));
      $this->messenger->addError($message);
      return FALSE;
    }

    curl_close($curl);

    return $result;
  }

}
