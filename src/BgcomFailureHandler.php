<?php

declare(strict_types = 1);

/**
 * @file
 * A simple logic to handle failures.
 */

namespace Drupal\bgcom_payment_gateway;

use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;

trait BgcomFailureHandler {

  /**
   * Handling failure cases at a central place.
   *
   * @param string $message
   *   (optional) If enough is to display the same message to both end-users and
   *   site admins then callers can provide the string.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Redirects the visitor to the initial phase of the checkout process: the cart page.
   */
  protected function _failureHandler(string $message = '') : RedirectResponse {
    if ($message) {
      $this->logger->error($message);
      $this->messenger->addError($this->t($message));
    }
    $return_url = Url::fromRoute('commerce_cart.page', [], ['absolute' => TRUE])->toString();
    return new RedirectResponse($return_url);
  }

}
