<?php

declare(strict_types = 1);

namespace Drupal\bgcom_payment_gateway\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\commerce_payment\Entity\EntityWithPaymentGatewayInterface;

/**
 * Provides an interface defining the “business day closure” custom content entity type.
 */
interface BusinessDayClosureInterface extends ContentEntityInterface, EntityChangedInterface, EntityWithPaymentGatewayInterface {

  // @todo decide whether do we need to store general return values as constants or not?

  /** @var string Status code received in the response to a successful business day closure request. */
  const RESPONSE_SUCCESS_CODE = "500";

  /** @var string Value received in the response to a successful business day closure request. */
  const RESPONSE_SUCCESS = "OK";

  /** @var string Value received in the response to an unsuccessful business day closure request. */
  const RESPONSE_FAILURE = "FAILED";

  /**
   * Calculates the exact date of the business day to which the closure refers.
   *
   * @return string
   *   A Unix timestamp of the last second of the given business day.
   */
  public function getDateOfBusinessDay();
}
