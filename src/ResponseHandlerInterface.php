<?php

declare(strict_types = 1);

namespace Drupal\bgcom_payment_gateway;

interface ResponseHandlerInterface {

  /**
   * A utility tool that simply should not exist in the API-first era we are
   * living nowadays.
   *
   * @param string $response
   *   A raw string to be processed.
   * @param string $command
   *   A single-char indication of calling which command resulted the received response.
   *
   * @return array|false
   *   Associative array of parsed data or false if the response is about an error.
   *
   * @throws \InvalidArgumentException
   *   Thrown when the response string received for processing has zero char length.
   */
  public function handleResponse(string $response, string $command);

}
