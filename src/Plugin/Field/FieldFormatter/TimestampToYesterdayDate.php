<?php

declare(strict_types = 1);

namespace Drupal\bgcom_payment_gateway\Plugin\Field\FieldFormatter;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A field formatter plugin that calculates the date of the previous day from a
 * given timestamp value.
 *
 * @FieldFormatter(
 *   id = "timestamp_to_yesterday_date",
 *   label = @Translation("Timestamp to date of yesterday"),
 *   field_types = {
 *     "timestamp",
 *     "created",
 *     "changed",
 *   }
 * )
 */
class TimestampToYesterdayDate extends FormatterBase implements ContainerFactoryPluginInterface {

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * Constructs a TimestampToYesterdayDate object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    $label,
    $view_mode,
    array $third_party_settings,
    DateFormatterInterface $date_formatter
  ) {
    parent::__construct($plugin_id,
      $plugin_definition,
      $field_definition,
      $settings,
      $label,
      $view_mode,
      $third_party_settings
    );

    $this->dateFormatter = $date_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('date.formatter')
    );
  }

  public function viewElements(FieldItemListInterface $items, $langcode) {
    if (empty($items)) {
      return [];
    }

    $elements = [];

    /** @var \Drupal\Core\Field\Plugin\Field\FieldType\TimestampItem $item */
    foreach ($items as $item) {
      $field_value = $item->getValue();
      $timestamp = $field_value['value'];
      // @todo Unite this calculation logic with \Drupal\bgcom_payment_gateway\Entity\BusinessDayClosure::getDateOfBusinessDay().
      // To be sure to avoid timezone calculating errors, check the previous mid-day.
      $last_noon = (floor($timestamp/86400)*86400)-43200;
    }

    // @todo make date format configurable for this field formatter plugin.
    $elements[] = [
      '#markup' => $this->dateFormatter->format($last_noon, 'html_date'),
    ];

    return $elements;
  }

}
