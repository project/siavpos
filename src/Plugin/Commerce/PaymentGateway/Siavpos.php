<?php

declare(strict_types = 1);

/**
 * @file
 * Contains the payment gateway plugin for Drupal Commerce. Most of the business
 * logic happens in the class annotated under the `offsite-payment` key.
 */

namespace Drupal\bgcom_payment_gateway\Plugin\Commerce\PaymentGateway;

use Drupal\Core\Form\FormStateInterface;
use Drupal\bgcom_payment_gateway\BgcomFailureHandler;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\commerce_price\Price;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the plugin type of the "SIA Virtual POS" payment gateway.
 *
 * @\Drupal\commerce_payment\Annotation\CommercePaymentGateway(
 *   id = "siavpos",
 *   label = @Translation("SIA Virtual POS"),
 *   display_label = @Translation("SIA Virtual POS"),
 *   forms = {
 *     "offsite-payment" = "Drupal\bgcom_payment_gateway\PluginForm\SiavposRedirectForm",
 *   },
 *   payment_method_types = {"credit_card"},
 *   credit_card_types = {"mastercard", "visa"},
 * )
 */
class Siavpos extends OffsitePaymentGatewayBase implements SiavposInterface {

  use BgcomFailureHandler;

  /**
   * The logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The request handler service.
   *
   * @var \Drupal\bgcom_payment_gateway\RequestHandlerInterface
   */
  protected $requestHandler;

  /**
   * The response handler service.
   *
   * @var \Drupal\bgcom_payment_gateway\ResponseHandlerInterface
   */
  protected $responseHandler;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    $instance = parent::create(
      $container,
      $configuration,
      $plugin_id,
      $plugin_definition
    );
    $instance->logger = $container->get('logger.channel.bgcom_payment_gateway');
    $instance->messenger = $container->get('messenger');
    $instance->requestHandler = $container->get('bgcom_payment_gateway.request_handler');
    $instance->responseHandler = $container->get('bgcom_payment_gateway.response_handler');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
        'ecomm_server_url' => 'https://vpos.te.sia.eu:8443/ecomm/MerchantHandler',
        'ecomm_client_url' => 'https://vpos.te.sia.eu/ecomm/ClientHandler',
        'cert_url' => '/var/www/html/domain/certs/1234567keystore.pem',
        'currency' => '348'
      ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {

    $form = parent::buildConfigurationForm($form, $form_state);

    $form['ecomm_server_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('ECOMM server URL'),
      '#description' => $this->t('Electronic Commerce System server URL.'),
      '#default_value' => $this->configuration['ecomm_server_url'],
      '#required' => TRUE,
    ];

    $form['ecomm_client_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('ECOMM client URL'),
      '#description' => $this->t('Electronic Commerce System client URL.'),
      '#default_value' => $this->configuration['ecomm_client_url'],
      '#required' => TRUE,
    ];

    $form['cert_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Certification URL'),
      '#description' => $this->t('URL of the certification.'),
      '#default_value' => $this->configuration['cert_url'],
      '#required' => TRUE,
    ];

    $form['cert_pass'] = [
      '#type' => 'password',
      '#title' => $this->t('Keystore file password'),
      '#description' => $this->t('Password of the keystore file.'),
      '#default_value' => $this->configuration['cert_pass'],
      '#required' => TRUE,
    ];

    $form['currency'] = [
      '#type' => 'select',
      '#options' => [978 => 'EUR', 840 => 'USD', 941 => 'RSD', 703 => 'SKK',
        440 => 'LTL', 233 => 'EEK', 643 => 'RUB', 891 => 'YUM', 348 => 'HUF'],
      '#title' => $this->t('Currency'),
      '#description' => $this->t('Currency used for transactions.'),
      '#default_value' => $this->configuration['currency'],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $values = $form_state->getValue($form['#parents']);

    foreach (['ecomm_server_url', 'ecomm_client_url', 'cert_url', 'cert_pass', 'currency'] as $setting_name) {
      $this->configuration[$setting_name] = $values[$setting_name];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function refundPayment(PaymentInterface $payment_entity, Price $amount = NULL) {
    $this->assertPaymentState($payment_entity, ['completed', 'partially_refunded']);
    // If not specified, refund the entire amount.
    $amount = $amount ?: $payment_entity->getAmount();
    $this->assertRefundAmount($payment_entity, $amount);
    $post_data = [
      'command' => 'r',
      'trans_id' => $payment_entity->getRemoteId(),
      'amount' => $amount->getNumber() . '00',
    ];
    $response = $this->requestHandler->sendRequest($post_data);

    if (!$response) {
      return $this->_failureHandler('Call of the external payment_entity gateway service returned with an unspecified error.');
    }

    $response_content_deserialized = $this->responseHandler->handleResponse($response, $post_data['command']);

    if ($response_content_deserialized['RESULT'] !== 'REVERSED') {
      return $this->_failureHandler(sprintf('The refund process has finished successfully, but the transaction resulted a failure. The result code is: %s', $response_content_deserialized['RESULT_CODE']));
    }

    // Determine whether payment_entity has been fully or partially refunded.
    $old_refunded_amount = $payment_entity->getRefundedAmount();
    $new_refunded_amount = $old_refunded_amount->add($amount);
    if ($new_refunded_amount->lessThan($payment_entity->getAmount())) {
      $payment_entity->setState('partially_refunded');
    }
    else {
      $payment_entity->setState('refunded');
    }

    $payment_entity->setRefundedAmount($new_refunded_amount);
    $payment_entity->save();
  }

  /**
   * {@inheritdoc}
   */
  public function createPayment(PaymentInterface $payment, $capture = TRUE) {
    // @todo Implement createPayment() method.
  }

  /**
   * {@inheritdoc}
   */
  public function deletePaymentMethod(PaymentMethodInterface $payment_method) {
    // @todo Implement deletePaymentMethod() method.
  }

}
