<?php

declare(strict_types = 1);

namespace Drupal\bgcom_payment_gateway\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsRefundsInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsStoredPaymentMethodsInterface;

interface SiavposInterface extends OffsitePaymentGatewayInterface, SupportsRefundsInterface, SupportsStoredPaymentMethodsInterface {

}
