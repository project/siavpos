<?php

declare(strict_types = 1);

/**
 * @file
 * A centralized parser for processing response body values received as raw strings.
 */

namespace Drupal\bgcom_payment_gateway;

use Drupal\Core\Messenger\MessengerInterface;
use InvalidArgumentException;
use Psr\Log\LoggerInterface;

class ResponseHandler implements ResponseHandlerInterface {

  /**
   * The logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * {@inheritdoc}
   */
  public function __construct(LoggerInterface $logger, MessengerInterface $messenger) {
    $this->logger = $logger;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public function handleResponse(string $response, string $command) {

    if (strlen($response) < 1 || strlen($command) !== 1) {
      throw new InvalidArgumentException('Cannot process a zero-length response string.');
    }

    // First, verify validity of the response string.
    $entire_patterns = [
      'x' => '/trans_id=(?:[A-Za-z0-9+\/]{4})*(?:[A-Za-z0-9+\/]{2}%3D%3D|[A-Za-z0-9+\/]{3}%3D)?&Ucaf_Cardholder_Confirm=\d&language=\w{2}/',
      'v' => '/^TRANSACTION_ID: (?:[A-Za-z0-9+\/]{4})*(?:[A-Za-z0-9+\/]{2}==|[A-Za-z0-9+\/]{3}=)?$/',
      'c' => '/RESULT: \w+ RESULT_CODE: \d+ 3DSECURE: \w+ (?:3DSECURE_REASON: \d+ )?RRN: \d{12} APPROVAL_CODE: \d{6} CARD_NUMBER: \*{15,}/',
      'r' => '/^RESULT: \w+ RESULT_CODE: \d+$/',
      'b' => '/RESULT: [A-Z]+ RESULT_CODE: \d{3} FLD_074: \d+ FLD_075: \d+ FLD_076: \d+ FLD_077: \d+ FLD_086: \d+ FLD_087: \d+ FLD_088: \d+ FLD_089: \d+/',
    ];

    if (preg_match('/^(error|warning):\s/', $response)) {
      $message = 'Call of the external payment gateway service returned with an unspecified error.';
      $this->logger->error($message . ' The response was: ' . $response);
      $this->messenger->addError($message);
      return FALSE;
    }

    $is_valid = FALSE;

    if (preg_match($entire_patterns[$command], $response) !== 1) {
      $this->logger->error('The following raw string received as input does not match the syntax pattern designated by the command "%command": %response', [
        '%command' => $command,
        '%response' => $response,
      ]);

      foreach ($entire_patterns as $pattern) {
        if (preg_match($pattern, $response)) {
          $is_valid = TRUE;
          break;
        }
      }

      if (!$is_valid) {
        $this->logger->error(sprintf('The following raw string received as input did not match any of the known syntax patterns: %s', $response));
      }
    }

    // Then start processing.
    switch ($command) {
      case 'x':
        $process_pattern = '/(?<key>\w+)(?:\=|\&?)(?<value>[A-Za-z0-9%+,.-]*)/m';
        break;

      default:
        $process_pattern = '/(?<key>\w+):\s(?<value>\S+)/m';
    }

    preg_match_all($process_pattern, $response, $matches);

    for ($i = 0; $i < count($matches[0]); $i++) {
      $response_content_deserialized[$matches['key'][$i]] = $matches['value'][$i];
    }

    return $response_content_deserialized ?: FALSE;
  }

}
