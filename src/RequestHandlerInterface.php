<?php

declare(strict_types = 1);

namespace Drupal\bgcom_payment_gateway;

interface RequestHandlerInterface {

  /**
   * @param array $post_data
   *   Associative array of the following values necessary to be set for an external API call:
   *     - 'command':
   *       - 'v' for the initial registration of a newly created transaction
   *       - 'c' for verifying the results of an already existing transaction
   *       - 'r' for refunding a transaction
   *       - 'b' for initiating a business day closure
   *
   *   When 'command' is set to 'v' then array items are expected to be:
   *     - 'amount'
   *     - 'currency'
   *     - 'client_ip_addr'
   *     - 'description'
   *     - 'language'
   *
   *   When 'command' is set to 'c' then array items are expected to be:
   *     - 'trans_id'
   *     - 'client_ip_addr'
   *
   *   When 'command' is set to 'r' then array items are expected to be:
   *     - 'trans_id'
   *     - 'amount'
   *
   *   When 'command' is set to 'b' then no further array items are needed.
   *
   *   In \Drupal\bgcom_payment_gateway\ResponseHandler::handleResponse() '$entire_patterns'
   *   array has an item on key 'x'. This is not a real command as others are but a placeholder
   *   only to differentiate processing of HTTP parameter string from raw plain text strings.
   *
   * @return string|false
   *   The response received from the call or false if any error occurred.
   *
   * @see \curl_exec
   */
  public function sendRequest(array $post_data);

}
